/* Autor: Simon Eder
 * Datum: 14.03.18
 *
 * return 0 == Erfolgreich
 * return 1 == Fileerror
 * return 2 == Fehler bei der Eingabe
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

void numbers(int count, int area, char *name){
	int i, range, x, y = 1, z = 0;
	FILE *fp;

	fp = fopen(name, "w");
	if(NULL == fp){
		perror("FILEERROR");
	}

	do{
		x = area/y;
		y = y * 10;
		z++;
	}while (1 <= x);
	range = z;

	srand(time(NULL));
	for (i = 0; i < count; i++){
		fprintf(fp, "%i\n", rand()%area);
	}
	fclose(fp);
}

int main(int argc, char *argv[]){
	char *ch = argv[1];

	int count = 20;
	int area = 1000;

	if (1 >= argc){
		printf("Keine Parameter erkannt!\n");
		return 2;
	}else if (2 <= argc && strchr(ch, 'h')){
		printf("\nCreatefile 'Operator' 'Parameter' 'Filename'\n\n");
		printf("default Anzahl		= 20\n");
		printf("default Zahlebereich	= 100\n\n");
		printf("Operatoren:\n");
		printf("   -n: 	Createfile -n	'Anzahl' 'Filename'\n");
		printf("   -r: 	Createfile -r	'Zahlenbereich' 'Filename'\n");
		printf("   -nr:	Createfile -nr	'Anzahl' 'Zahlenbereich' 'Filename'\n");
		printf("   -rn:	Createfile -rn	'Zahlenbereich' 'Anzahl' 'Filename'\n\n");
		return 2;
	}else if (2 == argc){
		if(0 == strcmp(ch, "-n")){
			printf("Geben Sie bitte eine Anzahl und einen Dateinamen an!\n");
			return 2;
		}else if (0 == strcmp(ch, "-r")){
			printf("Geben Sie bitte einen Zahlenbereich und einen Dateinamen an!\n");
			return 2;
		}else if (0 == strcmp(ch, "-nr")){
			printf("Geben Sie bitte Anzahl, Zahlenbereich und Dateiname an!\n");
			return 2;
		}else if (0 == strcmp(ch, "-rn")){
			printf("Geben Sie bitte Zahlenbereich, Anzahl und Dateiname an!\n");
			return 2;
		}else if (0 == strcmp(ch, "-h")){
			printf("Createfile 'Operator' 'Parameter' 'Filename'\n");
			printf("Operatoren:\n");
			printf("   -n: Anzahl der Zahlen angeben (default = 1oo)\n");
			printf("   -r: Zahelenbereich auswählen (default = 100)\n");
			return 2;
		}else{
			numbers(count, area, ch);
		printf("%i Zahlen von 0 bis %i wurden in %s gespeichert.\n", count, area-1, argv[1]);
			return 0;
		}
	}else if (3 == argc){
		printf("Fehler bei der Eingabe!\n");
		return 2;
	}else if (4 == argc){
		if (0 == strcmp(ch, "-n")){
			count = atoi(argv[2]);
			if (0 == count){
				printf("Fehler bei der Eingabe!\n");
				return 2;
			}
			numbers(count, area, argv[3]);
			printf("%i Zahlen von 0 bis %i wurden in %s gespeichert.\n", count, area-1, argv[3]);
			return 0;
		}else if (0 == strcmp(ch, "-r")){
			area = atoi(argv[2]);
			if (0 == area){
				printf("Fehler bei der Eingabe!\n");
				return 2;
			}
			numbers(count, area, argv[3]);
	                printf("%i Zahlen von 0 bis %i wurden in %s gespeichert.\n", count, area-1, argv[3]);
			return 0;
		}else{
			printf("Fehler bei der Eingabe!\n");
			return 2;
		}
	}else if (5 == argc){
		if(0 == strcmp(ch, "-nr")){
			count = atoi(argv[2]);
			area = atoi(argv[3]);
			if (0 == count || 0 == area){
				printf("Fehler bei der Eingabe!\n");
				return 2;
			}
			numbers(count, area, argv[4]);
	                printf("%i Zahlen von 0 bis %i wurden in %s gespeichert.\n", count, area-1, argv[4]);
			return 0;
		}else if (0 == strcmp(ch, "-rn")){
			count = atoi(argv[3]);
			area = atoi(argv[2]);
			if (0 == count || 0 == area){
				printf("Fehler bei der Eingabe!\n");
				return 2;
			}
			numbers(count, area, argv[4]);
	                printf("%i Zahlen von 0 bis %i wurden in %s gespeichert.\n", count, area-1, argv[4]);
			return 0;
		}else{
			printf("Fehler bei der Eingabe!\n");
			return 2;
		}
	}else{
		printf("Fehler bei der Eingabe!\n");
		return 2;
	}
}
