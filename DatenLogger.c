/* Autor: Simon Eder
 * Datum: 29.03.18
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

//Anzahl der Logs
int LOGS = 5;
//Wartezeit zwischen den logs in Sekunden
int SECS = 300;

struct tm *tmnow;

void temp(){
	float temp;
	int x;
	char ch[10];
	time_t tnow;
	FILE *fp;
	FILE *tmp;
	fp = fopen("data.log", "a");
	tmp = fopen("/sys/class/thermal/thermal_zone0/temp", "r");
	if(NULL == fp || NULL == tmp){
printf("Hier ist der Fehler!");
		perror("FILEERROR");
	}


	time(&tnow);
	tmnow = localtime(&tnow);
	fgets(ch, 10, tmp);
	x = atoi(ch);
	temp = x/1000;
	fprintf(fp, "Datum: %i.%i.%i	Zeit: %i:%i:%i		Temperatur:%.2f\n", tmnow->tm_mday, tmnow->tm_mon+1, tmnow->tm_year+1900, tmnow->tm_hour+2, tmnow->tm_min, tmnow->tm_sec, temp);
	fclose(fp);
}

int main(int argc, char *argv[]){
	int i;
	int zeit;

	if(2 == argc){
		printf("Geben Sie wie folgt ein:\n");
		printf("./DatenLogger 'Anhzahl der logs' 'Zeit in sec zwischen den logs'\n");
		printf("ODER\n");
		printf("./DatenLogger\n");
		printf("Standardwerte:\n\n");
		printf("Anzahl:	%i\n", LOGS);
		printf("Zeit:	%i\n", SECS);
		return 2;
	}else if(1 < argc){
		LOGS = atoi(argv[1]);
		SECS = atoi(argv[2]);
		if(0 == LOGS || 0 == SECS){
			printf("Fehler bei der Eingabe\n");
			return 2;
		}
	}

	for (i = 0; i < LOGS; i++){
		zeit = time(0) + SECS;
		temp();
		printf("%i. Messung durchgeführt. %i Messungen verbleibend(%i:%i min verbleibend).\n", i+1, LOGS-i-1, (((SECS*LOGS)-(SECS*i))/60), ((SECS*LOGS)-(SECS*i))-((((SECS*LOGS)-(SECS*i))/60)*60) );
		while(time(0) < zeit);
	}
}
