/*
 * Autor: Simon Eder
 * Datum: 11.03.18
 *
 * return 0 == Erfolgreich
 * return 1 == Fehler
 * return 2 == Fehler bei der Eingabe
 */

#include <stdio.h>
#include <string.h>
#include <wiringPi.h>

//Dateiname für die Morsezeichen
char code[] = "Morsecode";

//Pin auf der die LED steckt
int pin = 7;

//Länge eines dit
int dit = 250;

static const char* CHAR_TO_MORSE[128] = {
                NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                NULL, "-.-.--", ".-..-.", NULL, NULL, NULL, NULL, ".----.",
                "-.--.", "-.--.-", NULL, NULL, "--..--", "-....-", ".-.-.-", "-..-.",
                "-----", ".----", "..---", "...--", "....-", ".....", "-....", "--...",
                "---..", "----.", "---...", NULL, NULL, "-...-", NULL, "..--..",
                ".--.-.", ".-", "-...", "-.-.", "-..", ".", "..-.", "--.",
                "....", "..", ".---", "-.-", ".-..", "--", "-.", "---",
                ".--.", "--.-", ".-.", "...", "-", "..-", "...-", ".--",
                "-..-", "-.--", "--..", NULL, NULL, NULL, NULL, "..--.-",
                NULL, ".-", "-...", "-.-.", "-..", ".", "..-.", "--.",
                "....", "..", ".---", "-.-", ".-..", "--", "-.", "---",
                ".--.", "--.-", ".-.", "...", "-", "..-", "...-", ".--",
                "-..-", "-.--", "--..", NULL, NULL, NULL, NULL, NULL
};

//Nachricht zu Morsezeichen
void encode(char *name){
	int x;
	FILE *fp;
	FILE *dest;
	fp = fopen(name, "r");
	dest = fopen(code, "w");
	if(NULL == fp || NULL == dest){
		perror("FILEERROR");
	}

	while((x = fgetc(fp)) != EOF){
		if(32 == x){
			fprintf(dest, "\n");
		} else {
			fprintf(dest, "%s ", CHAR_TO_MORSE[x]);
		}
	}
	fclose(fp);
	fclose(dest);
}

//Morsezeichen zu Signal
void send(){
	int x;
	FILE *fp;
	fp = fopen(code, "r");
	if(NULL == fp){
		perror("FILEERROR");
	}

	while((x = fgetc(fp)) != EOF){
		//"." == 46 == dit
		if(46 == x){
			digitalWrite(pin, HIGH);
			delay(dit);
			digitalWrite(pin, LOW);
			delay(dit);
		//"." == 45 == dah
		}else if(45 == x){
			digitalWrite(pin, HIGH);
			delay(3 * dit);
			digitalWrite(pin, LOW);
			delay(dit);
		//" " == 32 == Pause zwischen Buchstaben
		}else if(32 == x){
			delay(2 * dit);
		//"\n" == 10 == Pause zwischen Wörtern
		}else if(10 == x){
			delay(6 * dit);
		}
	}
	fclose(fp);
}

//Main
int main(int argc, char *argv[]) {
        int i, j;

	if(1 >= argc){
		printf("Keine Parameter erkannt!\nBitte geben Sie einen Dateinamen an!\n\n");
		return 2;
	}

	//text in Datei zu Morse
	encode(argv[1]);

        //Pin configurieren
	wiringPiSetup();
	pinMode (pin, OUTPUT);

        //Morse ausgeben
	send();

	return 0;
}
