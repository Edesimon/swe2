/* Autor: Simon Eder
 * Datum: 19.03.18
 *
 * return 0 == Erfolg
 * return 1 == Fehler
 * return 2 == Fehler bei der Eingabe
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//zählt Anzahl
int CountFile(char *file){
	int len = 0;
	char cha[10000];
	FILE *fp;

	fp = fopen(file, "r");
	if (NULL == fp){
		perror("FILEERROR");
	}

	while(fgets(cha, 20, fp)){
		len++;
	}
	fclose(fp);
	return len;
}

//absteigend sortieren
void sort(char *file, char *name){
	int i, j, x;
	int len = CountFile(file);
	int MAX = 20;
	FILE *fp;
	FILE *dest;

	fp = fopen(file, "r");
	dest = fopen(name, "w");
	if (NULL == fp || NULL == dest){
		perror("FILEERROR");
	}

	//Zahlen in ein Array einlesen
	char ch[MAX];
	int *arr = malloc(len * sizeof(int));
	if (NULL == arr){
		free(arr);
		fclose(fp);
		fclose(dest);
		perror("ENOMEM");
	}
	for (i = 0; i < len ; i++){
		arr[i] = 0;
	}
	i = 0;
	while (fgets(ch, MAX, fp)){
		arr[i] = atoi(ch);
		i++;
	}

	//Zahlen sortieren
	for (i = len; i > 1; i--){
		for (j = 0; j < i-1; j++){
			if (arr[j] > arr[j+1]){
				x = arr[j];
				arr[j] = arr[j+1];
				arr[j+1] = x;
			}
		}
	}

	//Zahlen in die neue Datei einsortieren
	for (i = 0; i < len; i++){
		fprintf(dest, "%i\n", arr[i]);
	}

	free(arr);
	fclose(fp);
	fclose(dest);
}

//aufsteigend sortieren
void dsort(char *file, char *name){
	int i, j, x;
	int MAX = 20;
	int len = CountFile(file);
	FILE *fp;
	FILE *dest;

	fp = fopen(file, "r");
	dest = fopen(name, "w");
	if (NULL == fp || NULL == dest){
		perror("FILEERROR");
	}

	//Zahlen in ein Array einsortieren
	char ch[MAX];
	int *arr = malloc(len * sizeof(int));
	if (NULL == arr){
		perror("ENOMEM");
	}
	for(i = 0; i < len; i++){
		arr[i] = 0;
	}
	i = 0;
	while(fgets(ch, MAX, fp)){
		arr[i] = atoi(ch);
		i++;
	}

	//Zahlen sortieren
	for(i = len; i > 1; i--){
		for(j = 0; j < i-1; j++){
			if(arr[j] > arr[j+1]){
				x = arr[j];
				arr[j] = arr[j+1];
				arr[j+1] = x;
			}
		}
	}

	//Zahlen in das neue File schreiben
	for(i = len-1; i >= 0; i--){
		fprintf(dest, "%i\n", arr[i]);
	}

	free(arr);
	fclose(fp);
	fclose(dest);
}


int main(int argc, char* argv[]){
	int i;
	char *ch = argv[1];

	if (1 >= argc){
		printf("Keine Parameter erkannt!\n");
		return 2;
	}else if (strchr(ch, 'h')){
		printf("SortFile 'Option' 'Filename' 'Zieldateiname'\n\n");
		printf("Optionen:\n");
		printf("   -h == Hilfe\n");
		printf("   -a == Absteigend sortieren(default)\n");
		printf("   -d == Aufsteigend sortieren\n\n");
		return 0;
	}else if (3 == argc){
		sort(argv[1], argv[2]);
		return 0;
	}else if (4 <= argc){
		if(0 == strcmp(ch, "-a")){
			sort(argv[2], argv[3]);
			return 0;
		}else if (0 == strcmp(ch, "-d")){
			dsort(argv[2], argv[3]);
			return 0;
		}else{
			printf("Fehler bei der Eingabe!\n");
			return 2;
		}
	}else{
		printf("Fehler\n");
		return 1;
	}
}
